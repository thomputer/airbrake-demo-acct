require.config({
  paths: {
    airbrakeJs: 'node_modules/airbrake-js/dist'
  }
});

require(['airbrakeJs/client', 'airbrakeJs/instrumentation/jquery'],
        function (AirbrakeClient, instrumentJQuery) {
  var airbrake = new AirbrakeClient({
    projectId: 85155,
    projectKey: '7f8e100303d605e319792180457cfc58'});
  //airbrake.setHost('https://api.err.io');
  airbrake.addFilter(function(notice) {
    if (Math.floor((Math.random() * 5) + 1) == 1) {
      notice.context.environment = 'staging';
    } else if (Math.floor((Math.random() * 10) + 1) == 1) {
      notice.context.environment = 'development';
    } else {
      notice.context.environment = 'production';
    }

    var crumb = new Object();
    crumb['date'] = '2017-04-24T14:42:18.643Z';
    crumb['duration'] = 99;
    crumb['method'] = 'GET';
    crumb['statusCode'] = 200;
    crumb['type'] = 'xhr';
    crumb['url'] = '/api/v4/projects/123/lists/5678/new-items?key=absf134af44444ds324asdg32432';
    var crumb2 = new Object();
    crumb2['date'] = '2017-04-24T14:42:18.715Z';
    crumb2['type'] = 'error';
    crumb2['target'] = 'html > head > script[type=\"text/javascript\"][src=\"https://www.example.com/analytics.js\"]';
    var crumb21 = new Object();
    crumb21['date'] = '2017-04-24T14:42:18.799Z';
    crumb21['type'] = 'location';
    crumb21['to'] = '/projects/123/lists/9932/item/233332523';
    crumb21['from'] = '/projects/123/lists/5678/item/123536677';
    var crumb3 = new Object();
    crumb3['date'] = '2017-04-24T14:42:18.879Z';
    crumb3['type'] = 'click';
    crumb3['target'] = 'html.header > body.container > div.row.dashboard > button[name=\"expand\"]';
    var crumb4 = new Object();
    crumb4['date'] = '2017-04-24T14:42:19.003Z';
    crumb4['duration'] = 67;
    crumb4['method'] = 'POST';
    crumb4['statusCode'] = 0;
    crumb4['type'] = 'xhr';
    crumb4['url'] = 'https://api.segment.io/v1/p';
    var crumb5 = new Object();
    crumb5['date'] = '2017-04-24T14:42:20.234Z';
    crumb5['type'] = 'click';
    crumb5['target'] = 'html.header > body.container > div.row.dashboard > tab-ui-heading'
    var crumb6 = new Object();
    crumb6['date'] = '2017-04-24T14:42:20.379Z';
    crumb6['arguments'] = [notice.errors[0].type + ": " + notice.errors[0].message + "\n    at new b.component.controller (https://123.cloudfront.net/assets/project.js:22:1235)\n    at Object.l [as invoke] (https://123.cloudfront.net/assets/project.js:44:4235)\n    at fn (https://123.cloudfront.net/assets/project.js:8:295)\n    at c (https://123.cloudfront.net/assets/project.js:8:6695)", null];
    crumb6['severity'] = 'error';
    crumb6['type'] = 'log';
    notice.context.history = [crumb, crumb2, crumb21, crumb3, crumb4, crumb5, crumb6];

    notice.params.future_secret = "iajd23534hqtubawo3kor";
    notice.params.sub_id = 123142;
    notice.params.account_name = "beetboxesbots";
    var h = new Object();
    switch (Math.floor((Math.random() * 7) + 1)) {
      case (1):
        h['email'] = 'thom@acmecorp.com';
        h['id'] = '183127';
        h['name'] = 'Thom Example';
        break;
      case (2):
        h['email'] = 'nick@starkindustries.com';
        h['id'] = '234324';
        h['name'] = 'Nick Smith';
        break;
      case (3):
        h['email'] = 'mark@app.com';
        h['id'] = '333123';
        h['name'] = 'Mark Chang';
        break;
      case (4):
        h['email'] = 'rob@umbrella.corp';
        h['id'] = '443122';
        h['name'] = 'Robert Jackson';
        break;
      case (5):
        h['email'] = 'art@spacelysprockets.com';
        h['id'] = '123123';
        h['name'] = 'Art Burnside';
        break;
      case (6):
        h['email'] = 'arnold@skynet.org';
        h['id'] = '77788';
        h['name'] = 'Arnold Terminator';
        break;
      case (7):
        h['email'] = 'ellen@weyland-yutani.com';
        h['id'] = '23229';
        h['name'] = 'Ellen Ripley';
        break;
    }
    notice.context.user = h;

    var i;
    var filenames = new Object();
    filenames = ["app", "component/details", "component/account-view", "component/activity", "component/graph", "component/movement-speed", "component/picker-section"];
    var filename = filenames[(Math.floor((notice.errors[0].type.charCodeAt(0) % 13)))];
    console.log(notice.errors[0].type.charCodeAt(0));
    console.log(filename);
    for (i = 0; i < notice.errors[0].backtrace.length; i++) {
      notice.errors[0].backtrace[i].file = notice.errors[0].backtrace[i].file.replace(/file:\/\/\/Users\/thomascassady\/work\/sampleapps\/js_examples/i, "src");
      notice.errors[0].backtrace[i].file = notice.errors[0].backtrace[i].file.replace(/app/i, filename);
    }

    notice.context.url = "https://air-app-example.com/account/" + filename + ".html";
    notice.session.token = '1i3hu2351290502193jr02qxr33h0r820rh12r39';
    notice.session.account_auth_service_code = '9013243332';
    notice.params.arguments = ['state helper', 'first', 'success'];
    notice.params.status = '200';
    notice.params.response = 'redirecting to https://example.com/thing/12345';
    return notice;
  });

  if (window.jQuery) {
    instrumentJQuery(airbrake, jQuery);
  }

  today = new Date();
  day = today.getDate();
  rand = (Math.floor(day / 8) + 1);
  for (i = 0; i < 3 + rand; i++) {
    if (Math.floor((Math.random() * 3) + 1) == 1) {
      try {
        throw new ReferenceError("Color is not defined");
      } catch (err) {
        airbrake.notify(err);
      }

      if (day%5 == 0) {
        try {
          throw new Error("Unable to set property 'notifications' of undefined or null reference");
        } catch (err) {
          airbrake.notify(err);
        }
      }

      if (day%4 == 0) {
        try {
          throw new ReferenceError("jQuery is not defined");
        } catch (err) {
          airbrake.notify(err);
        }
      }

      if (day%3 == 0) {
        try {
          throw new ReferenceError("Can't find variable: nan");
        } catch (err) {
          airbrake.notify(err);
        }
      }

      if (day%2 == 0) {
        try {
          throw new TypeError("i is null");
        } catch (err) {
          airbrake.notify(err);
        }
      }
    }
    if (Math.floor((Math.random() * 16) + 1) == 1) {
      try {
        throw new TypeError("undefined is not an object");
      } catch (err) {
        airbrake.notify(err);
      }
    }
    if (Math.floor((Math.random() * 30) + 1) == 1) {
      try {
        throw new Error("too much recursion");
      } catch (err) {
        airbrake.notify(err);
      }
    }
    if (Math.floor((Math.random() * 14) + 1) == 1) {
      try {
        throw new RangeError("parameter must be between 1 and 100");
      } catch (err) {
        airbrake.notify(err);
      }
    }
    if (Math.floor((Math.random() * 63) + 1) == 1) {
      try {
        throw new SyntaxError("missing ; before statement");
      } catch (err) {
        airbrake.notify(err);
      }
    }
    if (Math.floor((Math.random() * 7) + 1) == 1) {
      try {
        throw new TypeError("null has no properties");
      } catch (err) {
        airbrake.notify(err);
      }
    }
    if (Math.floor((Math.random() * 9) + 1) == 1) {
      try {
        throw new Error("job 446653 could not be finished");
      } catch (err) {
        airbrake.notify(err);
      }
    }
    if (Math.floor((Math.random() * 40) + 1) == 1) {
      try {
        throw new Error("JSON Parse Error - Unexpected identifier \"field\"");
      } catch (err) {
        airbrake.notify(err);
      }
    }
  }

  if (day%2 == 0) {
    switch (Math.ceil(day/8)) {
      case (1):
        try {
          throw new SyntaxError("Unexpected identifier");
        } catch (err) {
          airbrake.notify(err);
        }
        break;
      case (2):
        try {
          throw new Error("InvalidAccessError");
        } catch (err) {
          airbrake.notify(err);
        }
        break;
      case (3):
        try {
          throw new ReferenceError("Can't find variable: $" + today.getMonth() + Math.ceil(day/8));
        } catch (err) {
          airbrake.notify(err);
        }
        break;
      case (4):
        try {
          throw new TypeError("Cannot read property 'FernetToken' of undefined");
        } catch (err) {
          airbrake.notify(err);
        }
        break;
    }
  }

  if (day%8 == 0) {
    try {
      throw new ReferenceError("remoteAddrs is not defined");
    } catch (err) {
      airbrake.notify(err);
    }
  }

  if (day%5 == 0) {
    try {
      throw new SyntaxError("Unexpected token < in JSON at position 0");
    } catch (err) {
      airbrake.notify(err);
    }
  }

  if (Math.ceil(day/8) >= 1) {
    try {
      throw new SyntaxError("Unexpected token < in JSON at position 0");
    } catch (err) {
      airbrake.notify(err);
    }
  }

  window.onerror = function(message, file, line) {
    airbrake.notify({error: {message: message, fileName: file, lineNumber: line}});
  }
});
